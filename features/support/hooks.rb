# frozen_string_literal: true
require "base64"
require 'selenium-webdriver'

Before do |scn|
  @home = Home.new
  @pagina_produtos = Produtos.new
  @carrinho = Carrinho.new
  @login = Login.new
  @checkout = Checkout.new
  @painel_franqueado = Homef.new
  @chopeira = Chopeira.new
  @cadastro = Cadastro.new
  @termos = Termos.new
end

After do |scn|
  binding.pry if ENV['debug']
end

After do
  shot_file = page.save_screenshot("log/screenshot.png")
  shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
  embed(shot_b64, "image/png", "Screenshot") # cucumber anexa o screenshot no report
end

#Para tirar print de quando o cenário falhar 

# after do |scenario|
#   if scenario.failed?
#     shot_file = page.save_screenshot("log/screenshot.png")
#     shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
#     embed(shot_b64, "image/png", "Screenshot") 
#   end
# end
