# Classe para validar cadastro de usuarios

class Cadastro 
    include RSpec::Matchers
    include Capybara::DSL

    def initialize 
        @entrar = EL['entrar']
        @criar_conta = EL['criar_conta']  
        @nome_conta = EL['nome_conta']
        @sobrenome_conta = EL['sobrenome_conta'] 
        @tel_conta  = EL['tel_conta']
        @cel_conta  = EL['cel_conta']
        @cpf_conta  = EL['cpf_conta']
        @data_nascimento = EL['data_nascimento']
        @sexo = EL['sexo']
        @time = EL['time'] 
        @info_adicionais = EL['info_adicionais'] 
        @email_conta = EL['email_conta']
        @email_confirma = EL['email_confirma']
        @cep_conta = EL['cep_conta']
        @num_residencia = EL['num_residencia']
        @senha = EL['senha']
        @confirmar_senha = EL['confirmar_senha']
        @concordo_termo_politica = EL['concordo_termo_politica']
        @fazer_cadastro = EL['fazer_cadastro']
    end

    def criar_conta
        find(@entrar).click
        find(@criar_conta).click
    end

    def email_invalido
        find(@email_conta).set('teste&&@gmail.com')
        find(@email_confirma).click
        assert_text('Informe um endereço de email válido.', wait: 10)
    end 

    def cadastro_customer
        cep = '04543000'
        data = '10101997'
        primeiro_nome = FFaker::Name.first_name.downcase
        sobrenome = FFaker::Name.last_name.downcase
        cpf = FFaker::IdentificationBR.cpf.to_s
        email =  "#{FFaker::Name.first_name.downcase}@mailinator.com"

        find(@entrar).click
        find(@criar_conta).click
        type_text(@nome_conta, text: primeiro_nome)
        type_text(@sobrenome_conta, text: sobrenome)
        find(@tel_conta).set('1111111111')
        find(@cel_conta).set('11111111111')
        find(@email_conta).set(email)
        find(@email_confirma).set(email)
        type_text(@cpf_conta, text: cpf) 
        type_text(@data_nascimento, text: data)  
        find(@sexo).select('Masculino')
        find(@time).select('São Paulo')
        find(@info_adicionais).select('Anúncios do Google')
        type_text(@cep_conta, text: cep) 
        find(@num_residencia).set('123')
        find(@senha).set('wes12345')
        find(@confirmar_senha).set('wes12345')
        find(@concordo_termo_politica).click
        sleep 3
        find(@fazer_cadastro).click
        assert_text('MEU PAINEL', wait: 10)
    end

    def cadastro_login

        cep = '04543000'
        data = '10101997'
        primeiro_nome = FFaker::Name.first_name.downcase
        sobrenome = FFaker::Name.last_name.downcase
        cpf = FFaker::IdentificationBR.cpf.to_s
        email =  "#{FFaker::Name.first_name.downcase}@mailinator.com"

        find(@criar_conta).click
        type_text(@nome_conta, text: primeiro_nome)
        type_text(@sobrenome_conta, text: sobrenome)
        find(@tel_conta).set('1111111111')
        find(@cel_conta).set('11111111111')
        find(@email_conta).set(email)
        find(@email_confirma).set(email)
        type_text(@cpf_conta, text: cpf) 
        type_text(@data_nascimento, text: data)  
        find(@sexo).select('Masculino')
        find(@time).select('São Paulo')
        find(@info_adicionais).select('Anúncios do Google')
        type_text(@cep_conta, text: cep) 
        find(@num_residencia).set('123')
        find(@senha).set('wes12345')
        find(@confirmar_senha).set('wes12345')
        find(@concordo_termo_politica).click
        sleep 3
        find(@fazer_cadastro).click
    end

    def type_text(element, text: nil)
        text.chars.each do |char|
            find(element).native.send_keys(char)
        end
    end

end