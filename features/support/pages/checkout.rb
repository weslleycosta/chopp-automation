# frozen_string_literal: true

# classe para validar dados do checkout
class Checkout
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @numero_endereco      = EL['numero_endereco']
    @complemento_endereco = EL['complemento_endereco']
    @referencia_endereco  = EL['referencia_endereco']
    @retira_loja          = EL['retira_loja']
    @pagamento_cartao     = EL['pagamento_cartao']
    @nome_cartao          = EL['nome_cartao']
    @numero_cartao        = EL['numero_cartao']
    @mes_validade         = EL['mes_validade']
    @ano_validade         = EL['ano_validade']
    @numero_verificacao   = EL['numero_verificacao']
    @numero_parcelas      = EL['numero_parcelas']
    @botao_finalizar      = EL['botao_finalizar']
    @declaro_aceito       = EL['declaro_aceito']
    @bandeira_cartao      = EL['bandeira_cartao']
    @icone_loading        = EL['icone_loading']
    @alterar_cep          = EL['alterar_cep']
    @novo_cep             = EL['novo_cep']
    @trocar_cep           = EL['trocar_cep']
    @endereco             = EL['endereco']
    @bairro               = EL['bairro']
    @clica_calendario     = EL['clica_calendario']
    @selecionando_data    = EL['selecionando_data']
    @pagamento_boleto     = EL['pagamento_boleto']
  end

  def dados_pessoais
    find(@numero_endereco).set('123')
    find(@complemento_endereco).set('casa 01')
    find(@referencia_endereco).set('teste')
  end

  def dados_endereco
    find(@endereco).set('Rua Santarem')
    find(@numero_endereco).set('123')
    find(@complemento_endereco).set('casa 01')
    sleep 2
    find(@bairro).set('demo')
  end

  def dados_entrega
    find(@retira_loja).click
    espera_ajax
  end

  def data_entrega
    find(@clica_calendario).click
    find(@selecionando_data).click
  end

  def dados_pagamento
    txt = '5503704647063112'

    find(@pagamento_cartao, wait: 10).click
    espera_ajax
    script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
    execute_script(script_de_troca_de_valores)
    find(@nome_cartao).set('Weslley Costa')
    txt.chars.each { |c| find(@numero_cartao).click.native.send_keys(c) }
    find(@mes_validade).select('01 - Janeiro')
    find(@ano_validade).select('2021')
    find(@numero_verificacao).set('123')
    find(@bandeira_cartao).click
  end

  def pagamento_credito
    txt = '5338538989883104'

    script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
    execute_script(script_de_troca_de_valores)
    find(@nome_cartao).set('Weslley Costa')
    txt.chars.each { |c| find(@numero_cartao).click.native.send_keys(c) }
    find(@mes_validade).select('01 - Janeiro')
    find(@ano_validade).select('2021')
    find(@numero_verificacao).set('123')
  end

  def pagamento_chopeira
    txt = '5338538989883401'

    script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
    execute_script(script_de_troca_de_valores)
    txt.chars.each { |c| find(@numero_cartao).click.native.send_keys(c) }
    find(@mes_validade).select('01 - Janeiro')
    find(@ano_validade).select('2021')
    find(@numero_verificacao).set('123')
    find(@bandeira_cartao).click
  end
  
  def cartao_invalido
    txt = '0000000000000006'

    find(@pagamento_cartao, wait: 10).click
    espera_ajax
    find(@nome_cartao).set('Weslley Costa')
    txt.chars.each { |c| find(@numero_cartao).click.native.send_keys(c) }
    find(@mes_validade).select('01 - Janeiro')
    find(@ano_validade).select('2021')
    find(@numero_verificacao).set('123')
    find(@bandeira_cartao).click
  end


  def dados_resumo
    find(@botao_finalizar).click
    espera_ajax
  end

  def espera_ajax
    assert_no_selector(@icone_loading)
  end

  def cartao_jcb
    txt = '3591073543306498'

    find(@pagamento_cartao, wait: 10).click
    espera_ajax
    script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
    execute_script(script_de_troca_de_valores)
    find(@nome_cartao).set('Weslley Costa')
    txt.chars.each { |c| find(@numero_cartao).native.send_keys(c) }
    find(@mes_validade).select('01 - Janeiro')
    find(@ano_validade).select('2021')
    find(@numero_verificacao).set('123')
  end

  def alterar_cep
    txt = '06711400'        
    find(@alterar_cep).click
    txt.chars.each do |c| 
    find(@novo_cep).native.send_keys(c)
    find(@trocar_cep).click
    end
  end

  def declaro_aceito
    find(@declaro_aceito).click
  end

  def pagamento_boleto
    find(@pagamento_boleto).click
  end

end

# script_de_troca_de_valores = "document.querySelectorAll(\"div[class='cc_card_option'] > input[type='radio']\").forEach(el => (el.value = '997'));"
# execute_script(script_de_troca_de_valores)