# frozen_string_literal: true

# classe para validar os dados da pagina de carrinho
class Carrinho
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @botao_finalizar_compra = EL['botao_finalizar_compra']
    @fechar_alerta          = EL['fechar_alerta']
    @remover_item           = EL['remover_item']
    @continuar_comprando    = EL['continuar_comprando']
    @cupom_carrinho         = EL['cupom_carrinho']
  end

  def finalizar_compra
    find(@botao_finalizar_compra).click
    find(@fechar_alerta).click
  end

  def remover
    find(@remover_item).click
  end

  def continuar_comprando
    find(@continuar_comprando).click
  end

  def cupom
    find(@cupom_carrinho).set('cbe38')
  end
end
