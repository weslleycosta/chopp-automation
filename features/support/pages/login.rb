# frozen_string_literal: true

# classe para validar dados de login
class Login
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_email      = EL['campo_email']
    @campo_senha      = EL['campo_senha']
    @botao_entrar     = EL['botao_entrar']
    @email_franqueado = EL['email_franqueado']
    @senha_franqueado = EL['senha_franqueado']
    @entrar_home      = EL['entrar_home']
  end

  def dados_login
    find(@campo_email).set('teste.hub@mailinator.com')
    find(@campo_senha).set('teste123')
  end

  def botao_entrar
    find(@botao_entrar).click
    sleep 3
  end

  def login_franqueado
    find(@email_franqueado).set('testefranqueado2@mailinator.com')
    find(@senha_franqueado).set('teste@123')
  end

  def login 
    find(@entrar_home).click
    find(@campo_email).set('teste.hub@mailinator.com')
    find(@campo_senha).set('teste123')
  end
end
