
# classe para validar os dados da classe 

class Termos
    include RSpec::Matchers
    include Capybara::DSL

    def initialize 
        @check_concordo = EL['check_concordo']
        @botao_recusar = EL['botao_recusar']
        @botao_aceitar = EL['botao_aceitar']
    end

    def confirmando_termos
        find(@check_concordo).click
        find(@botao_aceitar).click
    end
end



