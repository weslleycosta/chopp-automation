# frozen_string_literal: true

# classe para validar home franqueado
class Homef
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @clicando_pedidos   = EL['clicando_pedidos']
    @clicando_relatorio = EL['clicando_relatorio']
    @clicando_pedido    = EL['clicando_pedido']
    @alerta_home        = EL['alerta_home']
    @config_franquia    = EL['config_franquia']
    @campo_valor        = EL['campo_valor']
    @salvar_alteracao   = EL['salvar_alteracao']
    @confirmar_senha    = EL['confirmar_senha']
    @senha_configuracao = EL['senha_configuracao']
  end

  def clicar_relatorio
    find(@clicando_pedidos).click
    find(@clicando_relatorio).click
  end

  def clicar_pedido
    find(@clicando_pedidos).click
    find(@clicando_pedido).click
  end

  def fecha_alerta
    find(@alerta_home).click
  end

  def config_franquia
    find(@config_franquia).click
  end

  def valor_frete
    find(@campo_valor).set('145')
  end

  def frete_invalido
    find(@campo_valor).set('20000')
  end

  def botao_salvar
    find(@salvar_alteracao).click
  end

  def confirma_senha
    find(@senha_configuracao).set('expressmoema')
    find(@confirmar_senha).set('expressmoema')
  end
end
