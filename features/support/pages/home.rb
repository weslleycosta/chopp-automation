# frozen_string_literal: true

# classe para validar home
class Home
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_cep         = EL['campo_cep']
    @confirmando_idade = EL['confirmando_idade']
    @buscar_endereco   = EL['buscar_endereco']
    @fechar_alerta     = EL['fechar_alerta']
    @campo_chopeira    = EL['campo_chopeira']
    @entrar            = EL['entrar']
    @logo              = EL['logo']
  end

  def buscando_cep
    find(@buscar_endereco).click
  end
  
  def confirmando_idade
    find(@confirmando_idade, wait: 10).click
  end

  def digit_by_digit
    txt = '04543000'
    txt.chars.each { |c| find(@campo_cep).click.native.send_keys(c) }
  end

  def sem_logradouro
    txt = '13295000'
    txt.chars.each { |c| find(@campo_cep).click.native.send_keys(c) }
  end

  def fechando_alerta
    find(@fechar_alerta).click
  end

  def pagina_chopeira
    find(@campo_chopeira).click
  end

  def entrar
    find(@entrar).click
  end

  def logo
    find(@logo).click
  end
end
