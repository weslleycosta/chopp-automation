# frozen_string_literal: true

# classe para validar dados da pagina de produtos
class Produtos
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @clicando_produto        = EL['clicando_produto']
    @botao_adiciona_carrinho = EL['botao_adiciona_carrinho']
    @produto_aleatorio       = EL['produto_aleatorio']
  end

  def selecionando_item
    assert_selector(@clicando_produto, wait: 60)
    find(@clicando_produto).click
  end

  def item_aleatorio
    assert_selector(@produto_aleatorio, wait: 60)
    all(@produto_aleatorio).sample.click
  end

  def adicionando_carrinho
    assert_selector(@botao_adiciona_carrinho, wait: 60)
    find(@botao_adiciona_carrinho).click
  end
end
