
# classe para validar dados do checkout
class Chopeira
    include RSpec::Matchers
    include Capybara::DSL

    def initialize
        @comprar_agora = EL['comprar_agora']
        @finalizar_compra = EL['finalizar_compra']
        @alterar_cep = EL['alterar_cep']
        @novo_cep = EL['novo_cep']
        @trocar_cep = EL['trocar_cep']
        @informar_cep = EL['informar_cep']
    end

    def botao_comprar
        find(@comprar_agora).click
    end

    def finalizar_compra
        find(@finalizar_compra).click
    end

    def informar_cep
        txt = '04543000'
        
        find(@informar_cep).click
        txt.chars.each { |c| find(@informar_cep).click.native.send_keys(c) }
    end
end
