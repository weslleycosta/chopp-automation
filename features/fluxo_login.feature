#language: pt

    Funcionalidade: Fluxo de Login

    Contexto: Estar logado na página do chopp
        Dado que eu visito "chopp_brahma"
    
    Cenario: Logando no sistema
        E respondo que eu tenho mais de 18 anos
        Entao eu busco o cep de entrega

    @login_customer
    Cenario: Logando na conta
        E respondo que eu tenho mais de 18 anos
        Entao Clico em entrar e logo na conta do customer

    @teste17
    Cenario: Usuário é deslogado quando faz login e digita CEP
        E respondo que eu tenho mais de 18 anos
        Entao faço login e digito um cep de entrega