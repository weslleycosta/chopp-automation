# frozen_string_literal: true

Dado('preencho os dados do checkout e finalizo a compra') do
  @checkout.dados_pessoais
  @checkout.dados_entrega
  @checkout.dados_pagamento
  @checkout.declaro_aceito
  @checkout.dados_resumo
  assert_text('Recebemos seu pedido!')
end

Dado('preencho os dados do checkout e finalizo a compra com cep sem logradouro') do
  @checkout.dados_endereco
  @checkout.dados_entrega
  @checkout.pagamento_credito
  @checkout.declaro_aceito
  @checkout.dados_resumo
  assert_text('Recebemos seu pedido!')
end

Dado('preencho os dados do checkout e finalizo pedido') do
  @checkout.dados_pessoais
  @checkout.dados_entrega
  @checkout.pagamento_credito
  @checkout.declaro_aceito
  @checkout.dados_resumo
  assert_text('Recebemos seu pedido!')
end

Dado('preencho os dados do checkout com um cartao inválido e tento finalizar a compra') do
  @checkout.dados_pessoais
  @checkout.dados_entrega
  @checkout.cartao_invalido
  @checkout.dados_resumo
end

Dado('preencho os dados do checkout com um cartao JCB') do
  @checkout.dados_pessoais
  @checkout.dados_entrega
  @checkout.cartao_jcb
end

Dado('altero o cep de entrega, clico em finalizar pedido') do
  @checkout.alterar_cep
  @carrinho.finalizar_compra
end

Dado('preencho os dados do checkout e finalizo pedido com boleto') do
  @checkout.dados_pessoais
  @checkout.data_entrega
  @checkout.pagamento_boleto
  @checkout.declaro_aceito
  @checkout.dados_resumo
  assert_text('Boleto Bancário')
end