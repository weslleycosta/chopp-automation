# frozen_string_literal: true

Dado('digito os dados de login para acessar a minha conta') do
  @login.dados_login
  @login.botao_entrar
end

Dado('logo no painel do franqueado') do
  @login.login_franqueado
  @login.botao_entrar
end

Entao('faco login e finalizo a compra') do
  @login.dados_login
  @login.botao_entrar
  @checkout.dados_pessoais
  @checkout.pagamento_chopeira
  @checkout.dados_resumo
end

Entao('Clico em entrar e logo na conta do customer') do
  @home.entrar
  @login.dados_login
  @login.botao_entrar
end

Entao('faço login e digito um cep de entrega')do
  @login.login
  @login.botao_entrar
  @home.logo
  @home.digit_by_digit
  @home.buscando_cep
  assert_no_text('ENTRAR')
end