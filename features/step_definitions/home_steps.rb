# frozen_string_literal: true

Dado('que eu visito {string}') do |string|
  url = DATA[string]
  visit(url)
end

Dado('respondo que eu tenho mais de 18 anos') do
  @home.confirmando_idade
end

Dado('eu busco o cep de entrega') do
  @home.digit_by_digit
  @home.buscando_cep
  sleep 5
end

Dado('eu busco o cep de entrega sem logradouro') do
  @home.sem_logradouro
  @home.buscando_cep
end

Dado('fecho o alerta') do
  @home.fechando_alerta
end

Dado('fecho o alerta franqueados') do
  @painel_franqueado.fecha_alerta
end

Dado('clico em pedidos e relatorios') do
  @painel_franqueado.clicar_relatorio
end

Dado('clico em pedidos e pedidos')do
  @painel_franqueado.clicar_pedido
end

Entao("eu clico na chopeira para ir para a pagina") do
  @home.pagina_chopeira
end

Dado("clico em comprar e finalizar compra") do
  @chopeira.botao_comprar
  @chopeira.informar_cep
  @chopeira.finalizar_compra
end

