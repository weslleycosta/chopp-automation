

Entao('clico em configurações da franquia e configuro o frete') do
    @painel_franqueado.config_franquia
    @painel_franqueado.valor_frete
    @painel_franqueado.confirma_senha
    @painel_franqueado.botao_salvar
end

Entao('clico em configurações da franquia e digito um valor de frete maior que o configurado') do
    @painel_franqueado.config_franquia
    @painel_franqueado.frete_invalido
    @painel_franqueado.confirma_senha
    @painel_franqueado.botao_salvar
end

Entao('sistema deve exibir mensagem de Erro') do
    assert_text('Digite um valor igual ou menor a R$150,00', wait: 10) 
end
