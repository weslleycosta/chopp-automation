Dado('eu seleciono um produto e adiciono no carrinho') do
    @pagina_produtos.selecionando_item
    @pagina_produtos.adicionando_carrinho
end

Entao('eu removo o item do carrinho e continuo comprando') do
    @carrinho.remover
    @carrinho.continuar_comprando
end