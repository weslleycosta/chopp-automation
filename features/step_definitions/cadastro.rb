

Dado('faço cadastro de um customer')do
    @cadastro.cadastro_customer
end

Entao('eu clico em criar nova conta e finalizo o pedido') do
    @cadastro.cadastro_login
    @checkout.dados_pessoais
    @checkout.dados_entrega
    @checkout.dados_pagamento
    @checkout.dados_resumo
end

Entao('clico em entrar, criar nova conta') do
    @cadastro.criar_conta
end

Entao('preencho e-mail com caractere especial e sistema exibe mensagem') do
    @cadastro.email_invalido
end

Entao('crio uma nova conta') do
    @cadastro.cadastro_login
end
