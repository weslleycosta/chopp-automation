# frozen_string_literal: true

Quando('eu seleciono um produto') do
  @pagina_produtos.item_aleatorio
  @pagina_produtos.selecionando_item
end

Entao('adiciono ao carrinho e clico em finalizar compra') do
  @pagina_produtos.adicionando_carrinho
  @carrinho.finalizar_compra
end

Entao('adiciono ao carrinho, coloco um cupom de desconto e clico em finalizar compra') do
  @pagina_produtos.adicionando_carrinho
  @carrinho.cupom
  @carrinho.finalizar_compra
end