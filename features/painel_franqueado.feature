#language: pt

    Funcionalidade: Fluxo de Login

    Contexto: Estar logado na página do chopp
        Dado que eu visito "chopp_franqueado"

    @extrato
    Cenario: Relatorio mensal
        E logo no painel do franqueado
        E fecho o alerta franqueados
        Entao clico em pedidos e relatorios
    
    @login_franqueado
    Cenario: Logando no painel do franqueado
        Entao logo no painel do franqueado

    @consultar_pedido
    Cenario: Consultar pedido no painel do franqueado
        Dado logo no painel do franqueado
        E fecho o alerta franqueados
        Entao clico em pedidos e pedidos

    @config_frete
    Cenario: Configurar frete para zona não mapeada
        Dado logo no painel do franqueado
        E fecho o alerta franqueados
        Entao clico em configurações da franquia e configuro o frete

    @frete_erro
    Cenario: Erro ao tentar configurar frete com valor maior que configurado
        Dado logo no painel do franqueado
        E fecho o alerta franqueados
        E clico em configurações da franquia e digito um valor de frete maior que o configurado
        Entao sistema deve exibir mensagem de Erro