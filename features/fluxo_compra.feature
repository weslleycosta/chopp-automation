#language: pt

    Funcionalidade: Validar fluxo de compra

    Contexto: Realizar uma compra no sistema
        Dado que eu visito "chopp_brahma"

    @compra
    Cenario: Fluxo de compra
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout e finalizo a compra

    @compra_novo_usuario
    Cenario: Fluxo de compra
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E crio uma nova conta
        Entao preencho os dados do checkout e finalizo a compra
    
    @cartao_invalido
    Cenario: Fluxo de compra pedido cancelado gateway de pagamento
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout com um cartao inválido e tento finalizar a compra

    @oferta_customer
    Cenario: Selecionando oferta e criando customers
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E eu clico em criar nova conta e finalizo o pedido

    @compra_alterando_cep
    Cenario: Fluxo de compra alterando cep no checkout 
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        E altero o cep de entrega, clico em finalizar pedido
        Entao preencho os dados do checkout e finalizo pedido

    @cep_sem_logradouro
    Cenario: Fluxo de compra usando CEP sem logradouro
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega sem logradouro
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout e finalizo a compra com cep sem logradouro

    @fluxo_cupom
    Cenario: Fluxo de compra utilizando cupom no carrinho
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho, coloco um cupom de desconto e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout e finalizo a compra

    @fluxo_boleto
    Cenario: Realizar compra com boleto
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout e finalizo pedido com boleto