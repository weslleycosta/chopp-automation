#language: pt

    Funcionalidade: Cadastro de usuários

    Contexto: Estar na página do chopp
        Dado que eu visito "chopp_brahma"

    @cadastro
    Cenario: Cadastro de cliente
        E respondo que eu tenho mais de 18 anos
        Entao faço cadastro de um customer

    @email_caractere
    Cenario: Erro ao tentar cadastrar e-mail com caractere especial
        E respondo que eu tenho mais de 18 anos
        E clico em entrar, criar nova conta
        Entao preencho e-mail com caractere especial e sistema exibe mensagem
