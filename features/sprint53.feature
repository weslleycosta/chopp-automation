
#language: pt

    Funcionalidade: Validar cenarios de testes

    Contexto: Estar na página do chopp
        Dado que eu visito "chopp_brahma"

    @JCB
    Cenario: Validar bandeira JCB
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto
        Quando adiciono ao carrinho e clico em finalizar compra
        E digito os dados de login para acessar a minha conta
        Entao preencho os dados do checkout com um cartao JCB
