#language: pt

    Funcionalidade: Validar fluxo de compra

    Contexto: Realizar uma compra no sistema
        Dado que eu visito "chopp_brahma"

    @chopeira
    Cenario: Fluxo de compra chopeira
        E respondo que eu tenho mais de 18 anos
        Entao eu clico na chopeira para ir para a pagina
        E respondo que eu tenho mais de 18 anos
        Dado clico em comprar e finalizar compra
        Entao faco login e finalizo a compra
