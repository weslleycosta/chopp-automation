#language: pt

    Funcionalidade: Funcionalidades carrinho

    Contexto: Estar na página do chopp
        Dado que eu visito "chopp_brahma"

    @adicionando_produto
    Cenario: Adicionar produto no carrinho
        E respondo que eu tenho mais de 18 anos
        E eu busco o cep de entrega
        Entao eu seleciono um produto e adiciono no carrinho

    @removendo_produto
    Cenario: Removendo produto do carrinho
        E respondo que eu tenho mais de 18 anos
        Entao eu busco o cep de entrega
        Dado eu seleciono um produto e adiciono no carrinho
        Entao eu removo o item do carrinho e continuo comprando